package com.example.demo.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter{

	
	 	@Bean
	    public BCryptPasswordEncoder passwordEncoder() {
	        // パスワードの暗号化用に、bcrypt（ビー・クリプト）を使用します
	        return new BCryptPasswordEncoder();
	    }
	
	
	@Override
    protected void configure(HttpSecurity http) throws Exception {
		   http
		   // 認証リクエストの設定
	        .authorizeRequests()
	      
	        .antMatchers("/login").permitAll()
               // 認証の必要があるように設定
           .anyRequest().authenticated()
            .and() 
		   .formLogin()
           //ログイン処理のパス
           .loginProcessingUrl("/login")
           //ログインページ
           .loginPage("/login")
           //ログインエラー時の遷移先 ※パラメーターに「error」を付与
           .failureUrl("/login?error")
           //ログイン成功時の遷移先
           .defaultSuccessUrl("/", true)
           //ログイン時のキー：名前
           .usernameParameter("name")
           //ログイン時のパスワード
           .passwordParameter("password")
           .and()
           .logout()
           .logoutRequestMatcher(new AntPathRequestMatcher("/logout")) //
           .logoutUrl("/logout") //ログアウトのURL
           .logoutSuccessUrl("/login"); //ログアウト成功後のURL
	}

}
