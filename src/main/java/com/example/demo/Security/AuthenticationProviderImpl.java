package com.example.demo.Security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

@Component
public class AuthenticationProviderImpl implements AuthenticationProvider {

    @Autowired
    LoginUserDetailsService loginUserDetailsService;

	@Override
	public Authentication authenticate(Authentication authentication) throws AuthenticationException {
		  authentication.isAuthenticated();
	        String username = authentication.getName();
	        String password = authentication.getCredentials().toString();


	        UserDetails user = loginUserDetailsService.loadUserByUsername(username);

	        if(!user.getPassword().equals(password)) {
	            throw new AuthenticationCredentialsNotFoundException("アカウントまたはaaaパスワードが誤ってます");
	        }
	        UsernamePasswordAuthenticationToken authenticationResult
	        = new UsernamePasswordAuthenticationToken(user,authentication.getCredentials(),user.getAuthorities());

	        authenticationResult.setDetails(authentication.getDetails());
	        authenticationResult.isAuthenticated();
	                return authenticationResult;
	}

	@Override
	public boolean supports(Class<?> authentication) {
	       return UsernamePasswordAuthenticationToken.class
	                .isAssignableFrom(authentication);
	}



  

}