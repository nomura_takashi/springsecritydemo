package com.example.demo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class demoController {

	 @GetMapping("/")
	public ModelAndView getIndex() {
		ModelAndView mav = new ModelAndView();
		mav.setViewName("/top");
		
		return mav;
	}
	 
	 @GetMapping("/login")
	public ModelAndView getlogin() {
		ModelAndView mav = new ModelAndView();
		mav.setViewName("/login");
		
		return mav;
	}
	
}
